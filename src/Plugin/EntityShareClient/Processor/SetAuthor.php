<?php

declare(strict_types=1);

namespace Drupal\set_author\Plugin\EntityShareClient\Processor;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\entity_share\EntityShareUtility;
use Drupal\entity_share_client\ImportProcessor\ImportProcessorPluginBase;
use Drupal\entity_share_client\RuntimeImportContext;
use Drupal\entity_share_client\Service\RemoteManagerInterface;
use Drupal\user\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Set the author for a shared content entity.
 *
 * @ImportProcessor(
 *   id = "set_author",
 *   label = @Translation("Set author"),
 *   description = @Translation("Set the author of shared content entity to a pre-configured author."),
 *   stages = {
 *     "process_entity" = 110,
 *   },
 *   locked = false,
 * )
 */
class SetAuthor extends ImportProcessorPluginBase implements PluginFormInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The remote manager.
   *
   * @var \Drupal\entity_share_client\Service\RemoteManagerInterface
   */
  protected RemoteManagerInterface $remoteManager;

  /**
   * Constructor for the SetAuthor plugin.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\entity_share_client\Service\RemoteManagerInterface $remote_manager
   *   The remote manager service.
   */
  final public function __construct(array $configuration, string $plugin_id, array $plugin_definition, EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger, RemoteManagerInterface $remote_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->remoteManager = $remote_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('logger.channel.entity_share_client'),
      $container->get('entity_share_client.remote_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $default_user = User::getAnonymousUser();
    return [
      'shared_author' => $default_user->id(),
      'create_author' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $shared_author = User::load($this->configuration['shared_author']);
    $form['shared_author'] = [
      '#title' => $this->t('Shared author'),
      '#description' => $this->t(
        'This user will be used as the author of shared content entities by default if source author not exist in this site.'
      ),
      '#type' => 'entity_autocomplete',
      '#required' => TRUE,
      '#target_type' => 'user',
      '#default_value' => $shared_author,
      '#selection_settings' => [
        'include_anonymous' => TRUE,
      ],
    ];
    $form['create_author'] = [
      '#title' => $this->t('Create author if it does not yet exist'),
      '#description' => $this->t(
        'If the author assigned to a node does not yet exist, try to create that user locally when this is enabled.'
      ),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['create_author'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function processEntity(RuntimeImportContext $runtime_import_context, ContentEntityInterface $processed_entity, array $entity_json_data): void {
    if (!empty($entity_json_data['relationships']['uid'])) {
      $field_data = $entity_json_data['relationships']['uid'];
      $prepared_field_data = EntityShareUtility::prepareData($field_data['data'] ?? [[]])[0];

      // Get author id if exist.
      $author_id = $this->getExistingEntities($prepared_field_data ?? []);
      if ($author_id === FALSE && isset($field_data['links']['related']['href'])) {
        // Get author id from remote site.
        $author_id = $this->getUserId($runtime_import_context, $field_data['links']['related']['href']);
      }
      if ($author_id === FALSE) {
        // Set default author id.
        $author_id = $this->configuration['shared_author'];
      }

      if ($processed_entity->get('uid') !== $author_id) {
        $processed_entity->set('uid', $author_id);
      }
    }
  }

  /**
   * Helper function to get existing uid.
   *
   * @param array $data
   *   The JSON:API data for user.
   *
   * @return bool|string
   *   uid of existing user or False.
   */
  protected function getExistingEntities(array $data): bool|string {
    if (isset($data['id']) && $data['id'] !== 'missing') {
      try {
        $entity_storage = $this->entityTypeManager->getStorage('user');
        $existing_entity_id = $entity_storage->getQuery()
          ->accessCheck(FALSE)
          ->condition('uuid', $data['id'], '=')
          ->execute();

        if ($existing_entity_id) {
          return reset($existing_entity_id);
        }
      }
      catch (\Exception $e) {
        $log_variables = [];
        $log_variables['@msg'] = $e->getMessage();
        $this->logger->error('Caught exception trying to load existing entities. Error message was @msg', $log_variables);
      }
    }
    return FALSE;
  }

  /**
   * Helper function.
   *
   * @param \Drupal\entity_share_client\RuntimeImportContext $runtime_import_context
   *   The runtime import context.
   * @param string $url
   *   The URL to import.
   *
   * @return int|bool
   *   The user attributes.
   */
  protected function getUserId(RuntimeImportContext $runtime_import_context, string $url): bool|int {
    $json = $this->remoteManager->jsonApiRequest($runtime_import_context->getRemote(), 'GET', $url);
    if ($json->getStatusCode() < 200 || $json->getStatusCode() >= 300) {
      return FALSE;
    }
    $body = Json::decode((string) $json->getBody());

    // $body['data'] can be null in the case of
    // missing/deleted referenced entities.
    if (!isset($body['errors']) && !is_null($body['data']) && isset($body['data']['attributes'])) {
      $attributes = $body['data']['attributes'];
      if (isset($attributes['mail']) && $user = user_load_by_mail($attributes['mail'])) {
        return (int) $user->id();
      }
      if (isset($attributes['name']) && $user = user_load_by_name($attributes['name'])) {
        return (int) $user->id();
      }
      if (isset($attributes['name'], $attributes['mail']) && $this->configuration['create_author']) {
        $user = User::create([
          'name' => $attributes['name'],
          'mail' => $attributes['mail'],
          'status' => $attributes['status'] ?? FALSE,
        ]);
        try {
          $user->save();
          return (int) $user->id();
        }
        catch (EntityStorageException) {
          // Intentionally ignored.
        }
      }
    }
    return FALSE;
  }

}
